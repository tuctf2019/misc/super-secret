# Super Secret

## Challenge description
Something's blocking my flag from this file...

## Required files for hosting
`document.odt`

## Hosting instructions
- The included file should be distributed with the challenge description/flag submission platform 

## Hints
- These are blocked from running by default for good reason nowadays.

## Flag 
`TUCTF{ST0P_TRUST1NG_M4CR0S_FR0M_4N_UNKN0WN_S0URC3}`